fn get_wrapped_reversed_slice(vec: &Vec<u8>, mut start_idx: u8, len: u8) -> Vec<u8> {
    let mut result: Vec<u8> = vec![];
    let mut dec_len = len;
    loop {
        if dec_len == 0 {
            break;
        }
        result.push(vec[start_idx as usize]);
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        dec_len -= 1;
    }
    result.reverse();
    assert!(result.len() == len as usize);
    result
}

fn replace_elements(
    vec_to_repl: &mut Vec<u8>,
    elems_vec: &Vec<u8>,
    mut start_idx: u8,
    mut len: u8,
) {
    let mut count = 0;
    loop {
        if len == 0 {
            break;
        }
        vec_to_repl[start_idx as usize] = elems_vec[count];
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        len -= 1;
        count += 1;
    }
}

fn main() {
    let input_chars: Vec<char> = vec![
        '9', '4', ',', '8', '4', ',', '0', ',', '7', '9', ',', '2', ',', '2', '7', ',', '8', '1',
        ',', '1', ',', '1', '2', '3', ',', '9', '3', ',', '2', '1', '8', ',', '2', '3', ',', '1',
        '0', '3', ',', '2', '5', '5', ',', '2', '5', '4', ',', '2', '4', '3',
    ];
    let mut input: Vec<u8> = input_chars.iter().map(|x| *x as u8).collect();
    input.extend(&[17, 31, 73, 47, 23]);

    let mut starting_vec: Vec<u8> = (0..255).collect();
    starting_vec.push(255);

    let mut current_position: u32 = 0;
    let mut skip_size: u32 = 0;

    for _ in 0..64 {
        for num in &input {
            let slice = get_wrapped_reversed_slice(&starting_vec, current_position as u8, *num);
            replace_elements(&mut starting_vec, &slice, current_position as u8, *num);
            let tmp = *num as u32 + skip_size;
            current_position = (current_position + tmp) % 256;
            assert!(current_position <= 255);
            skip_size += 1;
        }
    }

    let mut output = "".to_string();
    let mut range_start = 0;

    while range_start < 256 {
        let mut sum: u8 = 0;
        for i in range_start..range_start + 16 {
            sum ^= starting_vec[i];
        }
        let hex_repr = format!("{:x}", sum);
        if hex_repr.len() == 1 {
            output.push('0')
        }
        output.push_str(&hex_repr);
        range_start += 16;
    }

    println!("{}", output);
}

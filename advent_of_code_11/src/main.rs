use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashSet;

fn main() {
    let mut v: Vec<i64> = vec![];
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    for line in f.lines() {
        let l = line.unwrap();
        let vec: Vec<&str> = l.split('\t').collect();
        for val in vec{
            v.push(val.parse::<i64>().unwrap());
        }
    }

    let mut uniques = HashSet::new();
    loop
    {
        //convert the numbers vector to a continuous string
        let mut s: String = "".to_string();
        for i in &v {
            s.push_str(&i.to_string());
            s.push('_');
        }
        if uniques.contains(&s)
        {
            break;
        }
        else
        {
            uniques.insert(s);
        }
        //spill the largest number
        let mut qwe = v.len() - 1 - v.iter().rev().enumerate().max_by(|x, y| {
        (x.1).cmp(y.1)}).unwrap().0;
        let initial_val = v[qwe];
        v[qwe] -= initial_val;
        for _ in (1..initial_val+1).rev() {
            qwe = (qwe + 1) % v.len();
            v[qwe] += 1;
        }
    }
    println!("{}", uniques.len());
}

use std::collections::HashMap;

#[derive(Hash, Eq, PartialEq, Copy, Clone, Debug)]
struct Coords {
    x: i64,
    y: i64,
}

fn get_sum(map: &HashMap<Coords, u64>, c: Coords) -> u64 {
    let mut sum: u64 = 0;
    sum += match map.get(&Coords {
        x: c.x - 1,
        y: c.y - 1,
    }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords { x: c.x - 1, y: c.y }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords {
        x: c.x - 1,
        y: c.y + 1,
    }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords { x: c.x, y: c.y - 1 }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords { x: c.x, y: c.y + 1 }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords {
        x: c.x + 1,
        y: c.y - 1,
    }) {
        Some(&c) => c,
        _ => 0,
    };
    sum += match map.get(&Coords { x: c.x + 1, y: c.y }) {
        Some(&c) => c,
        _ => 0,
    };
    let tmp_coords = Coords {
        x: c.x + 1,
        y: c.y + 1,
    };
    sum += match map.get(&tmp_coords) {
        Some(&c) => c,
        _ => 0,
    };
    return sum;
}

fn main() {
    let mut table: HashMap<Coords, u64> = HashMap::new();
    let mut last_coords = Coords { x: 0, y: 0 };
    table.insert(last_coords, 1);
    let mut n = 1;
    let mut latest_entry = 0;
    'outer: loop {
        let side_len = 2 * n + 1;
        // 1 right
        last_coords = Coords {
            x: last_coords.x + 1,
            y: last_coords.y,
        };
        let num = get_sum(&table, last_coords);
        if num > 277678 {
            latest_entry = num;
            break 'outer;
        }
        table.insert(last_coords, num);
        //side_len - 2 times up
        for _ in 1..side_len - 1 {
            last_coords = Coords {
                x: last_coords.x,
                y: last_coords.y - 1,
            };
            let num = get_sum(&table, last_coords);
            if num > 277678 {
                latest_entry = num;
                break 'outer;
            }
            table.insert(last_coords, num);
        }
        //side_len - 1 times left
        for _ in 1..side_len {
            last_coords = Coords {
                x: last_coords.x - 1,
                y: last_coords.y,
            };
            let num = get_sum(&table, last_coords);
            if num > 277678 {
                latest_entry = num;
                break 'outer;
            }
            table.insert(last_coords, num);
        }
        //side_len - 1 times down
        for _ in 1..side_len {
            last_coords = Coords {
                x: last_coords.x,
                y: last_coords.y + 1,
            };
            let num = get_sum(&table, last_coords);
            if num > 277678 {
                latest_entry = num;
                break 'outer;
            }
            table.insert(last_coords, num);
        }
        //side_len - 1 times right
        for _ in 1..side_len {
            last_coords = Coords {
                x: last_coords.x + 1,
                y: last_coords.y,
            };
            let num = get_sum(&table, last_coords);
            if num > 277678 {
                latest_entry = num;
                break 'outer;
            }
            table.insert(last_coords, num);
        }
        n += 1;
    }
    println!("{}", latest_entry);
}

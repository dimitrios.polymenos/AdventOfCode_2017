fn get_wrapped_reversed_slice(vec: &Vec<u8>, mut start_idx: u8, len: u8) -> Vec<u8> {
    let mut result: Vec<u8> = vec![];
    let mut dec_len = len;
    while dec_len != 0 {
        result.push(vec[start_idx as usize]);
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        dec_len -= 1;
    }
    result.reverse();
    result
}

fn replace_elements(
    vec_to_repl: &mut Vec<u8>,
    elems_vec: &Vec<u8>,
    mut start_idx: u8,
    mut len: u8,
) {
    let mut count = 0;
    while len != 0 {
        vec_to_repl[start_idx as usize] = elems_vec[count];
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        len -= 1;
        count += 1;
    }
}

fn knot_hash(input_string: &str) -> (u64, u64) {
    let mut input: Vec<u8> = input_string.as_bytes().iter().map(|x| *x as u8).collect();
    input.extend(&[17, 31, 73, 47, 23]);

    let mut starting_vec: Vec<u8> = (0..255).collect();
    starting_vec.push(255);

    let mut current_position: u32 = 0;
    let mut skip_size: u32 = 0;

    for _ in 0..64 {
        for num in &input {
            let slice = get_wrapped_reversed_slice(&starting_vec, current_position as u8, *num);
            replace_elements(&mut starting_vec, &slice, current_position as u8, *num);
            let tmp = *num as u32 + skip_size;
            current_position = (current_position + tmp) % 256;
            assert!(current_position <= 255);
            skip_size += 1;
        }
    }

    let mut output = "".to_string();
    let mut range_start = 0;

    while range_start < 256 {
        let mut sum: u8 = 0;
        for i in range_start..range_start + 16 {
            sum ^= starting_vec[i];
        }
        let hex_repr = format!("{:x}", sum);
        if hex_repr.len() == 1 {
            output.push('0')
        }
        output.push_str(&hex_repr);
        range_start += 16;
    }
    let first_slice = u64::from_str_radix(&output[0..16], 16).unwrap();
    let second_slice = u64::from_str_radix(&output[16..32], 16).unwrap();
    (first_slice, second_slice)
}

fn is_safe(values_matrix: &[[u8; 128]; 128], i: i64, j: i64) -> bool {
    (i >= 0) && (i < 128) && (j >= 0) && (j < 128) && (values_matrix[i as usize][j as usize] == 1)
}

fn dfs(values_matrix: &mut [[u8; 128]; 128], i: i64, j: i64) {
    const ROW_NBR: [i8; 4] = [-1, 1, 0, 0];
    const COL_NBR: [i8; 4] = [0, 0, 1, -1];

    values_matrix[i as usize][j as usize] = 0;
    for k in 0..4 {
        if is_safe(values_matrix, i + ROW_NBR[k] as i64, j + COL_NBR[k] as i64) {
            dfs(values_matrix, i + ROW_NBR[k] as i64, j + COL_NBR[k] as i64);
        }
    }
}

use std::fs::File;
use std::io::prelude::*;

fn main() {
    let input = "amgozmfv-";
    let mut sum = 0;
    let mut board: [[u8; 128]; 128] = [[0; 128]; 128];

    for i in 0..128 {
        let arranged_input = input.to_owned() + &i.to_string();
        let output = knot_hash(&arranged_input);
        sum += output.0.count_ones();
        sum += output.1.count_ones();
        let bin_repr = format!("{:064b}", output.0) + &format!("{:064b}", output.1);
        let input: Vec<u8> = bin_repr
            .chars()
            .map(|x| x.to_digit(10).unwrap() as u8)
            .collect();
        for j in 0..128 {
            board[i][j] = input[j];
        }
    }

    //problem 1
    println!("{}", sum);

    //problem 2
    let mut count = 0;
    for i in 0..128 {
        for j in 0..128 {
            if board[i][j] == 1 {
                dfs(&mut board, i as i64, j as i64);
                count += 1;
            }
        }
    }
    println!("{}", count);
}

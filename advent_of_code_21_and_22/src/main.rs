use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn axial_to_cube_coords(coords: &(i32, i32)) -> (i32, i32, i32) {
    let x = coords.0;
    let z = coords.1;
    let y = -x - z;
    (x, y, z)
}

fn cube_distance(a: &(i32, i32, i32), b: &(i32, i32, i32)) -> i32 {
    let tmp = std::cmp::max((a.0 - b.0).abs(), (a.1 - b.1).abs());
    std::cmp::max(tmp, (a.2 - b.2).abs())
}

fn main() {
    let f = File::open("input.txt").unwrap();
    let mut reader = BufReader::new(f);

    let mut directions_string = String::new();
    reader.read_to_string(&mut directions_string).unwrap();
    let directions: Vec<&str> = directions_string.split(',').collect();

    let mut coords = (0, 0);
    let mut max_distance = 0;
    for direction in directions {
        match direction {
            "n" => {
                coords.1 -= 1;
            }
            "ne" => {
                coords.0 += 1;
                coords.1 -= 1;
            }
            "se" => {
                coords.0 += 1;
            }
            "s" => {
                coords.1 += 1;
            }
            "sw" => {
                coords.0 -= 1;
                coords.1 += 1;
            }
            "nw" => {
                coords.0 -= 1;
            }
            _ => panic!("Oops, wrong input found"),
        }

        let tmp = cube_distance(
            &axial_to_cube_coords(&(0, 0)),
            &axial_to_cube_coords(&coords),
        );
        max_distance = if tmp > max_distance {
            tmp
        } else {
            max_distance
        };
    }
    //question 1
    println!(
        "{}",
        cube_distance(
            &axial_to_cube_coords(&(0, 0)),
            &axial_to_cube_coords(&coords)
        )
    );
    //question 2
    println!("{}", max_distance);
}

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug, PartialEq)]
struct State {
    num_groups: u64,
    score: u64,
    current_group_depth: i64,
    in_garbage: bool,
    discard_next: bool,
}

impl State {
    fn enter_group(&mut self) {
        if !self.in_garbage {
            self.current_group_depth += 1;
            self.num_groups += 1;
        }
    }
    fn leave_group(&mut self) {
        if !self.in_garbage {
            self.score += self.current_group_depth as u64;
            self.current_group_depth -= 1;
            assert!(self.current_group_depth >= 0);
        }
    }
}

const INITIAL_STATE: State = State {
    num_groups: 0,
    score: 0,
    current_group_depth: 0,
    in_garbage: false,
    discard_next: false,
};

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let mut f = BufReader::new(fi);
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();

    let mut group_state = INITIAL_STATE;
    let mut discarded_characters = 0;

    for token in contents.chars() {
        if !group_state.discard_next {
            if !group_state.in_garbage {
                match token {
                    '{' => group_state.enter_group(),
                    '}' => group_state.leave_group(),
                    '<' => group_state.in_garbage = true,
                    '>' => group_state.in_garbage = false,
                    '!' => group_state.discard_next = true,
                    _ => {}
                };
            } else {
                match token {
                    '>' => group_state.in_garbage = false,
                    '!' => group_state.discard_next = true,
                    _ => discarded_characters += 1,
                };
            }
        } else {
            group_state.discard_next = false;
        }
    }

    println!("total score: {}", group_state.score);
    println!("discarded characters: {}", discarded_characters);
}

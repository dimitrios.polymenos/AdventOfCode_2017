use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::FromIterator;

#[derive(Debug, Clone)]
struct Program {
    name: String,
    weight: i64,
    parent: Option<String>,
    children: Vec<String>,
    children_weights: Vec<i64>,
    depth: i64,
}

fn calculate_children_weights_recursive(
    program_name: String,
    map: &HashMap<String, Program>,
) -> Vec<i64> {
    let mut w: Vec<i64> = vec![];
    match map.get(&program_name) {
        Some(prog) => if prog.children.len() > 0 {
            for child in &prog.children {
                match map.get(child) {
                    Some(v) => {
                        let sum = calculate_children_weights_recursive(v.name.clone(), map)
                            .iter()
                            .fold(0, |sum, val| sum + val);
                        w.push(sum + v.weight);
                    }
                    None => {}
                };
            }
        },
        None => {}
    }
    w
}

fn calculate_node_depth(program_name: String, map: &HashMap<String, Program>, depth: i64) -> i64 {
    match map.get(&program_name) {
        Some(prog) => match prog.parent.clone() {
            Some(name) => calculate_node_depth(name.clone(), map, depth + 1),
            None => depth,
        },
        None => 0,
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut disc_map: HashMap<String, Program> = HashMap::new();
    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();
        let x: &[_] = &['(', ')'];
        let mut record = Program {
            name: line_description[0].to_string(),
            weight: line_description[1].trim_matches(x).parse().unwrap(),
            parent: None,
            children: vec![],
            children_weights: vec![],
            depth: 0,
        };
        record.children = line_description
            .into_iter()
            .skip(3)
            .map(|x| x.trim_matches(',').to_string())
            .collect();
        disc_map.insert(record.name.clone(), record);
    }

    let mut cloned_map: HashMap<String, Program> = disc_map.clone();
    for entry in &disc_map {
        for child in &entry.1.children {
            let x = entry.1.name.clone();
            match cloned_map.get_mut(child) {
                Some(v) => {
                    v.parent = Some(x);
                }
                None => {}
            };
        }
    }

    for entry in &mut disc_map {
        entry.1.depth = calculate_node_depth(entry.0.clone(), &cloned_map, 0);
    }

    for entry in &disc_map {
        let x = calculate_children_weights_recursive(entry.0.clone(), &disc_map);
        let set: HashSet<i64> = HashSet::from_iter(x.iter().cloned());
        let sum = x.iter().fold(0, |sum, val| sum + val);
        if entry.0 == "tlskukk" {
            println!("{} - 6 = {}", entry.1.weight, entry.1.weight - 6);
        }
    }
}

use std::fs::File;
use std::io::BufReader;
use std::io::prelude::BufRead;

use std::collections::HashMap;
use std::collections::VecDeque;

#[derive(Debug, Clone)]
enum Op {
    SndOp,
    SetOp,
    AddOp,
    MulOp,
    ModOp,
    RcvOp,
    JgzOp,
}

#[derive(Debug, Clone)]
enum Operand {
    Value(i64),
    Register(char),
}

#[derive(Debug, Clone)]
struct Instruction {
    operation: Op,
    left_operand: Operand,
    right_operand: Option<Operand>,
}

#[derive(Debug)]
struct ProgramState {
    map: HashMap<char, i64>,
    instructions: Vec<Instruction>,
    received_values: VecDeque<i64>,
    sent_values: VecDeque<i64>,
    instruction_index: i64,
    num_sent: i64,
    blocked: bool,
    finished: bool,
    id: u8,
}

impl ProgramState {
    fn new(program_id: u8) -> ProgramState {
       let mut r = ProgramState {
            map: HashMap::new(),
            instructions: vec![],
            received_values: VecDeque::new(),
            sent_values: VecDeque::new(),
            instruction_index: 0,
            num_sent: 0,
            blocked: false,
            finished: false,
            id: program_id,
        };
        r.map.insert('p', program_id as i64);
        r
    }

    fn process_instruction(&mut self) {
        let q = &self.instructions[self.instruction_index as usize];
        let operand_val = match &q.right_operand {
            &Some(ref operand) => match operand {
                &Operand::Value(v) => Some(v),
                &Operand::Register(r) => Some( *(self.map.entry(r).or_insert(0)) ),
            }
            &None => None,
        };

        let l_operand_val = match q.left_operand {
            Operand::Register(r) => *self.map.entry(r).or_insert(0),
            Operand::Value(v) => v,
        };

        let l_operand_reg_name = match q.left_operand {
            Operand::Register(r) => Some(r),
            Operand::Value(_) => None,
        };

        match &q.operation {
            &Op::SndOp => {
                self.sent_values.push_back(l_operand_val);
                self.num_sent += 1;
            }
            &Op::SetOp => {self.map.insert(l_operand_reg_name.unwrap(), operand_val.unwrap());}
            &Op::AddOp => {self.map.insert(l_operand_reg_name.unwrap(), l_operand_val + operand_val.unwrap());}
            &Op::MulOp => {self.map.insert(l_operand_reg_name.unwrap(), l_operand_val * operand_val.unwrap());}
            &Op::ModOp => {self.map.insert(l_operand_reg_name.unwrap(), l_operand_val % operand_val.unwrap());}
            &Op::RcvOp => {
                if self.received_values.is_empty() {
                    self.blocked = true;
                    return; // bail early, we blocked
                }
                else {
                    self.blocked = false;
                    let rec = self.received_values.pop_front().unwrap();
                    self.map.insert(l_operand_reg_name.unwrap(), rec);
                }
            }
            &Op::JgzOp => {self.instruction_index += if l_operand_val > 0 { operand_val.unwrap() - 1 } else { 0 };}
        }
        self.instruction_index += 1;
        if self.instruction_index < 0 || self.instruction_index >= self.instructions.len() as i64 {
            self.finished = true;
        }
    }

    fn reset(&mut self)
    {
        self.map.clear();
        self.map.insert('p', self.id as i64);
        self.received_values.clear();
        self.sent_values.clear();
        self.instruction_index = 0;
        self.num_sent = 0;
        self.blocked = false;
        self.finished = false;
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut state0: ProgramState = ProgramState::new(0);
    let mut state1: ProgramState = ProgramState::new(1);

    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();
        let instruction_op = match line_description[0] {
            "snd" => Op::SndOp,
            "set" => Op::SetOp,
            "add" => Op::AddOp,
            "mul" => Op::MulOp,
            "mod" => Op::ModOp,
            "rcv" => Op::RcvOp,
            "jgz" => Op::JgzOp,
            _ => panic!("Unexpected Instruction!"),
        };

        let l_operand: Operand = match line_description[1].chars().nth(0).unwrap() {
            'a'...'z' => Operand::Register(line_description[1].chars().nth(0).unwrap()),
            '0'...'9' | '-' => Operand::Value(line_description[1].parse::<i64>().unwrap()),
            _ => panic!("Unexpected Operand!"),
        };

        let r_operand: Option<Operand> = if line_description.len() == 3 {
            match line_description[2].chars().nth(0).unwrap() {
                'a'...'z' => Some(Operand::Register(line_description[2].chars().nth(0).unwrap())),
                '0'...'9' | '-' => {
                    Some(Operand::Value(line_description[2].parse::<i64>().unwrap()))
                }
                _ => panic!("Unexpected Operand!"),
            }
        } else {
            None
        };

        let instruction = Instruction {
            operation: instruction_op,
            left_operand: l_operand,
            right_operand: r_operand,
        };

        state0.instructions.push(instruction.clone());
        state1.instructions.push(instruction);
    }

    //problem 1
    while !state0.finished {
        let idx = state0.instruction_index;
        state0.process_instruction();
        if state0.instruction_index == idx {
            break;
        }
    }
    println!("{:?}", state0.sent_values.pop_back().unwrap());

    state0.reset();

    //problem 2
    loop {
        state0.process_instruction();
        while !state0.sent_values.is_empty() {
            state1.received_values.push_back(state0.sent_values.pop_front().unwrap());
        }

        state1.process_instruction();
        while !state1.sent_values.is_empty() {
            state0.received_values.push_back(state1.sent_values.pop_front().unwrap());
        }
        if (state0.blocked || state0.finished) && (state1.blocked || state1.finished) { break; }
    }
    println!("{}", state1.num_sent);
}

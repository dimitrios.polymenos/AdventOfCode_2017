use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashMap;

fn rotate(vector: &Vec<char>, rhs: usize) -> Vec<char> {
    let (a, b) = vector.split_at(vector.len() - rhs);
    let mut spun_vector: Vec<char> = vec![];
    spun_vector.extend_from_slice(b);
    spun_vector.extend_from_slice(a);
    spun_vector
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let mut f = BufReader::new(fi);
    let mut input: String = String::new();
    f.read_to_string(&mut input).unwrap();

    let original_values: Vec<char> = vec![
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'
    ];

    let input: Vec<&str> = input.split(',').collect();

    let mut dances_map: HashMap<String, usize> = HashMap::new();

    let mut values = original_values.clone();
    let mut i = 0 as usize;
    loop {
        for entry in &input {
            match entry.get(0..1).unwrap() {
                "s" => {
                    let val: usize = entry.get(1..).unwrap().parse().unwrap();
                    values = rotate(&values, val);
                }
                "x" => {
                    let val: Vec<&str> = entry.get(1..).unwrap().split('/').collect();
                    let left: usize = val[0].parse().unwrap();
                    let right: usize = val[1].parse().unwrap();
                    values.swap(left, right);
                }
                "p" => {
                    let val: Vec<&str> = entry.get(1..).unwrap().split('/').collect();
                    let left: usize = values
                        .iter()
                        .position(|x| x == &val[0].chars().next().unwrap())
                        .unwrap();
                    let right: usize = values
                        .iter()
                        .position(|x| x == &val[1].chars().next().unwrap())
                        .unwrap();
                    values.swap(left, right);
                }
                _ => panic!("Error, unexpected input!"),
            }
        }
        let result: String = values.iter().cloned().collect();
        if dances_map.contains_key(&result) {
            break;
        }
        dances_map.insert(result, i);
        i += 1;
    }

    let entry_val = 1000000000 % i;

    for entry in dances_map {
        if entry.1 == 0 {
            //problem 1
            println!("{}", entry.0);
        }
        if entry.1 == entry_val - 1 {
            //problem 2
            println!("{}", entry.0);
        }
    }
}

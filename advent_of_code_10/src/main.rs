use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let mut v: Vec<i64> = vec![];
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    for line in f.lines() {
        let l = line.unwrap();
        let s = l.parse::<i64>().unwrap();
        v.push(s);
    }

    let mut sum: u64 = 0;
    let mut idx: i64 = 0;
    loop {
        let steps_to_move = v[idx as usize];
        v[idx as usize] = if steps_to_move >= 3 {
            steps_to_move - 1
        } else {
            steps_to_move + 1
        };
        idx += steps_to_move;
        sum += 1;
        if idx > (v.len() - 1) as i64 {
            break;
        }
    }
    println!("{}", sum);
}

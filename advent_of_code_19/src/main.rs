fn get_wrapped_reversed_slice(vec: &Vec<u8>, mut start_idx: u8, len: u8) -> Vec<u8> {
    let mut result: Vec<u8> = vec![];
    let mut dec_len = len;
    loop {
        if dec_len == 0 {
            break;
        }
        result.push(vec[start_idx as usize]);
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        dec_len -= 1;
    }
    result.reverse();
    assert!(result.len() == len as usize);
    result
}

fn replace_elements(
    vec_to_repl: &mut Vec<u8>,
    elems_vec: &Vec<u8>,
    mut start_idx: u8,
    mut len: u8,
) {
    let mut count = 0;
    loop {
        if len == 0 {
            break;
        }
        vec_to_repl[start_idx as usize] = elems_vec[count];
        start_idx = ((start_idx as u32 + 1) % 256) as u8;
        len -= 1;
        count += 1;
    }
}

fn main() {
    let input: Vec<u8> = vec![
        94, 84, 0, 79, 2, 27, 81, 1, 123, 93, 218, 23, 103, 255, 254, 243
    ];

    let mut starting_vec: Vec<u8> = (0..255).collect();
    starting_vec.push(255);

    let mut current_position: u32 = 0;
    let mut skip_size: u8 = 0;

    for num in input {
        let slice = get_wrapped_reversed_slice(&starting_vec, current_position as u8, num);
        replace_elements(&mut starting_vec, &slice, current_position as u8, num);
        let tmp = num as u32 + skip_size as u32;
        current_position = (current_position + tmp) % 256;
        assert!(current_position <= 255);
        skip_size += 1;
    }
    println!("{}", starting_vec[0] as u32 * starting_vec[1] as u32);
}
